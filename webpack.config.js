const { resolve } = require('path');

const HtmlPlugin = require('html-webpack-plugin');
const CssPlugin = require('mini-css-extract-plugin');


module.exports = {
  entry: {
    app: resolve(__dirname, 'src', 'app.js')
  },
  output: {
    path: resolve(__dirname, 'public'),
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [CssPlugin.loader, 'css-loader', 'postcss-loader']
      }
    ]
  },
  plugins: [
    new HtmlPlugin({
      template: resolve(__dirname, 'src', 'index.html')
    }),
    new CssPlugin()
  ]
}